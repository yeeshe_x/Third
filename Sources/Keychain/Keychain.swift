//  Keychain.swift
//  Copyright © 2020 xiangqiancao. All rights reserved.

import SAMKeychain

final public class Keychain {
    
    private static let ServiceNamed = "Keychain_Service_Name"
    private static let PasswordKey = "Keychain_Passowrd"
    private static let AccountKey = "Keychain_Account"
    private static let TokenKey = "Keychain_Token"
    private static let UUIDKey = "Keychain_UUID"
    
    /// Store login password in keychain.
    public static var password: String? {
        get {
            return SAMKeychain.password(forService: ServiceNamed, account: PasswordKey)
        }
        
        set {
            if let _ = newValue, newValue!.count > 0 {
                SAMKeychain.setPassword(newValue!, forService: ServiceNamed, account: PasswordKey)
            } else {
                SAMKeychain.deletePassword(forService: ServiceNamed, account: PasswordKey)
            }
        }
    }
    
    /// Store login account in keychain.
    public static var account: String? {
        get {
            return SAMKeychain.password(forService: ServiceNamed, account: AccountKey)
        }
        
        set {
            if let _ = newValue, newValue!.count > 0 {
                SAMKeychain.setPassword(newValue!, forService: ServiceNamed, account: AccountKey)
            } else {
                SAMKeychain.deletePassword(forService: ServiceNamed, account: AccountKey)
            }
        }
    }
    
    /// Store access token in keychain.
    public static var token: String? {
        get {
            return SAMKeychain.password(forService: ServiceNamed, account: TokenKey)
        }
        
        set {
            if let _ = newValue, newValue!.count > 0 {
                SAMKeychain.setPassword(newValue!, forService: ServiceNamed, account: TokenKey)
            } else {
                SAMKeychain.deletePassword(forService: ServiceNamed, account: TokenKey)
            }
        }
    }
    
    /// Get the device UUID in keychain. If not in keychain, create it.
    public static var UUID: String {
        guard let uuid = SAMKeychain.password(forService: ServiceNamed, account: UUIDKey) else {
            let uuid = CFUUIDCreate(nil)!
            let string = CFUUIDCreateString(nil, uuid)!
            SAMKeychain.setPassword(String(string), forService: ServiceNamed, account: UUIDKey)
            return String(string)
        }
        return uuid
    }
    
    /// Get the login state by using token and password.
    public static var isLogin: Bool {
        if password != nil && token != nil {
            return true
        }
        return false
    }
    
}
