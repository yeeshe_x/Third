//  Cell+Rx.swift
//  Copyright © 2020 xiangqiancao. All rights reserved.

import RxCocoa
import RxSwift

/// Observale for invoked `prepareForReuse`.
extension Reactive where Base: UICollectionViewCell {
    
    public var reused: Observable<Void> {
        return base.rx
            .methodInvoked(#selector(base.prepareForReuse))
            .map { _ in () }
    }
    
}

extension Reactive where Base: UITableViewCell {
    
    public var reused: Observable<Void> {
        return base.rx
            .methodInvoked(#selector(base.prepareForReuse))
            .map { _ in () }
    }
    
}
