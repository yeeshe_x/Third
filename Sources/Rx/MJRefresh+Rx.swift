//  MJRefresh+Rx.swift
//  Copyright © 2020 xiangqiancao. All rights reserved.

import MJRefresh
import RxCocoa
import RxSwift

extension Reactive where Base: MJRefreshComponent {
    
    /// Observable of begin refreshing.
    public var beginRefreshing: Observable<Void> {
        return Observable.create { [weak component = self.base] observer in
            MainScheduler.ensureRunningOnMainThread()

            guard let component = component else {
                observer.onCompleted()
                return Disposables.create()
            }
            
            component.beginRefreshingCompletionBlock = {
                observer.onNext(())
            }
            
            return Disposables.create {
                component.beginRefreshingCompletionBlock = nil
                observer.onCompleted()
            }
        }
    }
    
}
