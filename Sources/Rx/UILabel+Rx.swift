//  UILabel+Rx.swift
//  Copyright © 2020 xiangqiancao. All rights reserved.

import RxCocoa
import RxSwift

extension Reactive where Base: UILabel {
    
    /// Bindable sink for `textColor` property.
    public var textColor: Binder<UIColor?> {
        return Binder(self.base) { label, textColor in
            label.textColor = textColor
        }
    }
    
}

