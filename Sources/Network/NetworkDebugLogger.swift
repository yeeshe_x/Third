//  NetworkDebugLogger.swift
//  Copyright © 2020 xiangqiancao. All rights reserved.

import Moya
import Result

public final class NetworkDebugLogger: PluginType {
    
    private let loggerStart = "\n*************************************** HTTP Debug Logger Start ***************************************\n"
    private let loggerEnd   = "\n**************************************** HTTP Debug Logger End ****************************************\n"
    private let responseMid = "\n************************************* HTTP Debug Logger Response **************************************\n"
    private let separator = ", "
    private let terminator = "\n"
    private let requestDataFormatter: ((Data) -> (String))?
    private let responseDataFormatter: ((Data) -> (Data))?
    private var outputs = [String: [String]] ()
    
    public init(requestDataFormatter: ((Data) -> (String))? = nil,
                responseDataFormatter: ((Data) -> (Data))? = nil
    ) {
        self.requestDataFormatter = requestDataFormatter
        self.responseDataFormatter = responseDataFormatter
    }
    
    #if DEBUG
    public func willSend(_ request: RequestType, target: TargetType) {
        var output = [loggerStart]
        output += logNetworkRequest(request.request as URLRequest?)
        outputs[targetID(target)] = output
    }

    public func didReceive(_ result: Result<Moya.Response, MoyaError>, target: TargetType) {
        let tID = targetID(target)
        var output = outputs[tID] ?? []
        if output.count == 0 {
            output += [loggerStart]
        }
        output += [responseMid]
        if case .success(let response) = result {
            output += logNetworkResponse(response.response, data: response.data, target: target)
        } else {
            output += logNetworkResponse(nil, data: nil, target: target)
        }
        output += [loggerEnd]
        outputItems(output)
        outputs.removeValue(forKey: tID)
    }
    #endif
    
}

fileprivate extension NetworkDebugLogger {
    
    private func targetID(_ target: TargetType) -> String {
        let baseURL = target.baseURL
        let fullURLString = baseURL.appendingPathComponent(target.path).absoluteString
        let targetID = fullURLString + target.method.rawValue
        return targetID
    }
    
    private func format(_ identifier: String, message: String) -> String {
        return "\(identifier): \(message)"
    }
    
    private func logNetworkRequest(_ request: URLRequest?) -> [String] {
        var output = [String]()
        // URL.
        output += [format("URL", message: request?.description ?? "(invalid request)")]
        // Headers.
        if let headers = request?.allHTTPHeaderFields {
            output += [format("Headers", message: headers.description)]
        }
        // Body Stream.
        if let bodyStream = request?.httpBodyStream {
            output += [format("Body Stream", message: bodyStream.description)]
        }
        // Method.
        if let httpMethod = request?.httpMethod {
            output += [format("Method", message: httpMethod)]
        }
        // Body.
        if let body = request?.httpBody, let stringOutput = requestDataFormatter?(body) ?? String(data: body, encoding: .utf8) {
            output += [format("Body", message: stringOutput)]
        }

        return output
    }
    
    func logNetworkResponse(_ response: HTTPURLResponse?, data: Data?, target: TargetType) -> [String] {
        guard let _ = response else {
           return [format("Response", message: "Received empty network response for \(target).")]
        }

        var output = [String]()
        // Description.
        // output += [format("Response", message: response.description)]
        // Result data.
        if let data = data, let stringData = String(data: responseDataFormatter?(data) ?? data, encoding: String.Encoding.utf8) {
            output += [stringData]
        }
        return output
    }
    
    private func outputItems(_ items: [String]) {
        items.forEach { output(separator, terminator: terminator, items: $0) }
        
    }
    
    private func output(_ separator: String, terminator: String, items: Any...) {
        items.forEach { print($0, separator: separator, terminator: terminator) }
    }
    
}
