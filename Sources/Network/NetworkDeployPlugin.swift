//  NetworkDeployPlugin.swift
//  Copyright © 2020 xiangqiancao. All rights reserved.

import Moya
import Result

/// Deploy request settings closure.
public typealias DeployRequestClosure = (_ request: URLRequest, _ target: TargetType) -> URLRequest

final public class NetworkDeployPlugin: PluginType {
    
    private let deployClosure: DeployRequestClosure
    
    public init(_ closure: @escaping DeployRequestClosure) {
        deployClosure = closure
    }
    
    public func prepare(_ request: URLRequest, target: TargetType) -> URLRequest {
        deployClosure(request, target)
    }
    
}
