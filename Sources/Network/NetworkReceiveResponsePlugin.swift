//  NetworkReceiveResponsePlugin.swift
//  Copyright © 2020 xiangqiancao. All rights reserved.

import Moya
import Result

public typealias ReceiveResponseClosure = (_ result: Result<Moya.Response, MoyaError>) -> Void

final public class NetworkReceiveResponsePlugin: PluginType {
    
    private let receiveResponse: ReceiveResponseClosure
    
    public init(_ closure: @escaping ReceiveResponseClosure) {
        receiveResponse = closure
    }
    
    public func didReceive(_ result: Result<Moya.Response, MoyaError>, target: TargetType) {
        receiveResponse(result)
    }

}
