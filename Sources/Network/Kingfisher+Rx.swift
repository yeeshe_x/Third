//  Kingfisher+Rx.swift
//  Copyright © 2020 xiangqiancao. All rights reserved.

import RxCocoa
import RxSwift
import Kingfisher

extension UIImage {
    
    /// Download UIImage from remote url. And cache on disk by using url as key.
    /// - Parameter string: The remote url string.
    /// - Returns: The singal of catch image result.
    public func image(fromURL string: String) -> Driver<UIImage?> {
        return Observable<UIImage?>.create { (anyObserver) -> Disposable in
            guard let url = URL(string: string) else {
                return Disposables.create()
            }
            
            var task: RetrieveImageDownloadTask?
            if let image = KingfisherManager.shared.cache.retrieveImageInDiskCache(forKey: string) {
                anyObserver.onNext(image)
            } else {
                task = KingfisherManager.shared.downloader.downloadImage(with: url) { (image, error, _, _) in
                    if image != nil {
                        KingfisherManager.shared.cache.store(image!, forKey: string)
                    }
                    anyObserver.onNext(image)
                }
            }
            
            return Disposables.create {
                task?.cancel()
            }
        }
        .asDriver(onErrorJustReturn: nil)
    }
    
}
