//  TargetType+Defaults.swift
//  Copyright © 2020 xiangqiancao. All rights reserved.

import Moya

public extension TargetType {
    
    var sampleData: Data { Data() }
    
    var headers: [String: String]? { return nil }
    
}
