//  Observable+HandyJSON.swift
//  Copyright © 2020 xiangqiancao. All rights reserved.

import RxSwift
import HandyJSON
import Moya

extension ObservableType where E == Response {
    
    public func mapModel<T: HandyJSON>(_ type: T.Type) -> Observable<T?> {
        return flatMap { response -> Observable<T?> in
            return Observable.just(response.mapModel(T.self))
        }
    }
    
}

extension Response {
    
    public func mapModel<T: HandyJSON>(_ type: T.Type) -> T? {
        let jsonString = String.init(data: data, encoding: .utf8)
        guard let object = JSONDeserializer<T>.deserializeFrom(json: jsonString) else {
            return nil
        }
        return object
    }
    
}
