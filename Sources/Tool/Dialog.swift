//  Dialog.swift
//  Copyright © 2020 xiangqiancao. All rights reserved.

import MBProgressHUD
import ObjectiveC

/// Make a toast with text in view. Hide after delay with duration.
/// - Parameters:
///   - text: Toast message.
///   - view: Super view.
///   - duration: Hide delay duration.
public func Toast(_ text: String, in view: UIView,
                   hideAfterDelay duration: TimeInterval = 2.689) {
    let toast = MBProgressHUD.showAdded(to: view, animated: true)
    toast.mode = .text
    toast.animationType = .fade
    toast.contentColor = .white
    toast.bezelView.style = .solidColor
    toast.bezelView.color = UIColor(white: 0.124, alpha: 0.848)
    toast.margin = 8
    toast.isSquare = false
    toast.isUserInteractionEnabled = false
    toast.label.numberOfLines = 0
    toast.label.text = text
    toast.label.font = UIFont.systemFont(ofSize: 14)
    toast.removeFromSuperViewOnHide = true
    toast.hide(animated: true, afterDelay: duration)
}

/// Make dialog with text in view.
/// - Parameters:
///   - text: Dialog text.
///   - view: Super view.
///   - masked: Have a masked background view.
public func Dialog(_ text: String, in view: UIView, is masked: Bool = false) {
    guard let dialog = view.dialog else {
        let dialog = MBProgressHUD.showAdded(to: view, animated: true)
        dialog.mode = .indeterminate
        dialog.animationType = .zoom
        dialog.contentColor = .white
        dialog.bezelView.blurEffectStyle = .dark
        dialog.bezelView.color = UIColor(white: 0.124, alpha: 0.848)
        if masked {
            dialog.backgroundColor = UIColor(white: 0.124, alpha: 0.342)
        }
        dialog.margin = 12
        dialog.isSquare = false
        dialog.isUserInteractionEnabled = true
        dialog.label.text = text
        dialog.label.font = UIFont.systemFont(ofSize: 14)
        for subview in dialog.bezelView.subviews {
            if let activityIndicatorView = subview as? UIActivityIndicatorView {
                activityIndicatorView.style = .white
                break
            }
        }
        dialog.removeFromSuperViewOnHide = true
        view.dialog = dialog
        return
    }
    dialog.label.text = text
}

/// Dismiss dialog in view.
/// - Parameter view: The dialog's super view.
public func Dialog(dismissIn view: UIView) {
    let dialog = view.dialog ?? MBProgressHUD.forView(view)
    dialog?.hide(animated: true)
    view.dialog = nil
}

/// Handle view's dialog property.
fileprivate extension UIView {
    
    private static var kDialogInstanceAssociateValue = 0
    
    var dialog: MBProgressHUD? {
        get {
            return objc_getAssociatedObject(self, &UIView.kDialogInstanceAssociateValue) as? MBProgressHUD
        }
        
        set {
            objc_setAssociatedObject(self, &UIView.kDialogInstanceAssociateValue, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
}
