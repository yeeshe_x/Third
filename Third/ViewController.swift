//  ViewController.swift
//  Copyright © 2020 xiangqiancao. All rights reserved.

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        Toast("【IOS】【订单详情-资质】", in: view)
        Dialog("加载中...", in: view)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            Dialog(dismissIn: self.view)
        }
    }


}

