Pod::Spec.new do |s|
  s.name         = "YSThird"
  s.version      = "0.0.6"
  s.summary      = "Commonly used tripartite libraries and their extensions and tools."
  s.description  = <<-DESC
  Commonly used tripartite libraries and their extensions and tools. Only used for Yeeshe.
                   DESC
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author             = { "Yeeshe" => "qiancaoxiang@163.com" }
  s.platform     = :ios, "8.0"
  s.homepage     = 'https://gitee.com/yeeshe_x/Third.git'
  s.source       = { :git => "https://gitee.com/yeeshe_x/Third.git", :tag => s.version }
  s.swift_version   = '5.0'
 
  s.frameworks   = 'UIKit', 'Foundation'
  s.dependency 'Moya/RxSwift', '~>13.0.1'
  s.dependency 'RxCocoa'
  
  s.subspec 'Network' do |ss|
    ss.dependency 'Kingfisher'
    ss.dependency 'HandyJSON'
    ss.source_files = 'Sources/Network/'
  end
  
  s.subspec 'Keychain' do |ss|
    ss.dependency 'SAMKeychain'
    ss.source_files = 'Sources/Keychain/'
  end

  s.subspec 'Rx' do |ss|
    ss.dependency 'MJRefresh'
    ss.source_files = 'Sources/Rx/'
  end

  s.subspec 'Tool' do |ss|
    ss.dependency 'MBProgressHUD'
    ss.source_files = 'Sources/Tool/'
  end

end
